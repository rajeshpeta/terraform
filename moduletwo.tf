##################################
## variables
################################

variable "aws_access_key"  {
  default = "AKIAJ6C467EDEQQTSHBA"
}
variable "aws_secret_key" {
  default = "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"
}

variable "private_key_path" {
   default = "/opt/keys/Awskey.pem"
}
variable "key_name" {
   default = "Awskey"
}

variable "region" {
  default = "ap-south-1"
}

variable "network_address_space" {
  default = "10.1.0.0/16"
}

variable "subnet1_address_space" {
  default = "10.1.1.0/24"
}

variable "subnet2_address_space" {
  default = "10.1.0.0/24"
}

 # Define the Provider/Cloud Provider

 provider "aws" {
   access_key = var.aws_access_key
   secret_key = var.aws_secret_key
   region = var.region
 }

#########################################################
# Data #
#########################################################

data "aws_availability_zones" "available" {}

data "aws_ami" "aws-ubuntu" {
most_recent = true
owners = ["amazon"]

filter {

  name = "name"
  values = ["amzn-ami-hvm*"]
}

filter {

  name = "root-device-type"
  values = ["ebs"]
}

filter {

  name = "virtualization-type"
  values = ["hvm"]
}
}

##########################################################
# Resources
##########################################################

# NETWORKING #

resource "aws_vpc" "Terr_VPC" {
   cidr_block = var.network_address_space
   enable_dns_hostnames = "true"
}

resource "aws_internet_gateway" "igw" {
     vpc_id = aws_vpc.Terr_VPC.id
}

resource "aws_subnet" "subnet1" {
    cidr_block = var.subnet1_address_space
    vpc_id = aws_vpc.Terr_VPC.id
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_subnet" "subnet2" {
    cidr_block = var.subnet2_address_space
    vpc_id = aws_vpc.Terr_VPC.id
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[1]
}
 # Routing #

 resource "aws_route_table" "rtb" {
   vpc_id = aws_vpc.Terr_VPC.id

         route {
            cidr_block = "0.0.0.0/0"
            gateway_id = aws_internet_gateway.igw.id
         }
}

resource "aws_route_table_association" "rta-subnet1" {
    subnet_id  = aws_subnet.subnet1.id
    route_table_id = aws_route_table.rtb.id
}

resource "aws_route_table_association" "rta-subnet2" {
    subnet_id  = aws_subnet.subnet2.id
    route_table_id = aws_route_table.rtb.id
}

# Elastic Load balancer security group #

resource "aws_security_group" "elb_sg" {
    name = "ubuntu-elb"
    vpc_id = aws_vpc.Terr_VPC.id

    # Allow HTTP from anywhere #

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
# Allow all outbound #

egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
}

# terraform Security group #
resource "aws_security_group" "Terr_sg" {
    name = "Terr_sg"
    vpc_id = aws_vpc.Terr_VPC.id

# SSH Access from anywhere#

  ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
}

# Http access from anywhere #

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = [var.network_address_space]
}

# outbound internet access #

  egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   }
}
### Load Balancer Setup ##

resource "aws_elb" "web" {
  name = "ubuntu-elb"

  subnets = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
  security_groups = [aws_security_group.elb_sg.id]
  instances = [aws_instance.Dev1.id, aws_instance.Dev2.id]

  listener {
     instance_port = 80
     instance_protocol = "http"
     lb_port = 80
     lb_protocol = "http"

  }

}

# Instnaces ##

     resource "aws_instance" "Dev1" {
     ami = "ami-0123b531fc646552f"
     instance_type = "t2.micro"
     subnet_id = aws_subnet.subnet1.id
     vpc_security_group_ids = [aws_security_group.Terr_sg.id]
     tags = {
       Name = "Dev1"
     }

     key_name = var.key_name

     connection {
        type = "ssh"
        host = self.public_ip
        user = "ubuntu"
        private_key = file(var.private_key_path)
     }
provisioner "remote-exec" {
   inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install tree"
    ]
}
}

resource "aws_instance" "Dev2" {
ami = "ami-0123b531fc646552f"
instance_type = "t2.micro"
subnet_id = aws_subnet.subnet2.id
vpc_security_group_ids = [aws_security_group.Terr_sg.id]
tags = {
  Name = "Dev2"
}

key_name = var.key_name

connection {
   type = "ssh"
   host = self.public_ip
   user = "ubuntu"
   private_key = file(var.private_key_path)
}
provisioner "remote-exec" {
inline = [
 "sudo apt-get -y update",
 "sudo apt-get -y install tree"
]
}
}

#########################################################
# OUTPUT
###########################################################
 output "aws_elb_public_dns" {
    value = aws_elb.web.dns_name
}