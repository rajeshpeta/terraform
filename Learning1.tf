###########working with variable in terraforms###############
Name, type, default
# Specify default variable and type

variable "environment_name" {
  type = string
  default = "development"

}

# Specify variable  in file

environment_name = "uat"

#Specify variable in inline

terraform plan -var 'environment_name=production'

# Create variable map

variable "cidr" {

 type = map(stirng)
 default = {
   development = "10.0.0.0/16"
   uat = "10.1.0.0/16"
   production = "10.2.0.0/16"

 }
}

# Multiple Environments #

# Create a workspace to perform actions in terffaform

terraform workspace new development

terraform workspace new uat

terraform workspace new production


# Managing Secrets####

$env:AWS_ACCESS_KEY_ID="ajsdhjkasbdjkasbndjkbn"
$env:AWS_SECRET_ACCESS_KEY="ajsdbkajsbfkjbasdfjkb"
