### All the way to start Terraform Need to provide "provider" which means which cloud service we need perform  Infrastructure as a code ####

provider "aws" {
    access_key = "my access key"
    secret_key = "my security key"
    region = "ap-south-1"
}

########## provider resource details which means instance type and key name security groups#########

resource "aws_instance" "webserver" {
         ami = "ami-id"
         instance_type = "t2.micro"
         key_name = "Awskey"
         vpc_security_group_ids = ["sg-id"]
         tags = {
            Name = "terraform-instance"
          }       
}
################### user_data the list of packages to be installed or execute commands in new terraform-instance ###########

user_data = <<-EOF

#!/bin/bash

sudo apt-get -y update
sudp apt-get install tree

 EOF

###################################################################
# Get into Existing Machines and execute operation
#####################################################################

provisioner "file" {
  connection {
    type = "ssh"
    user = "root"
    private_key = var.private_key
    host = var.hostname
  }
source = "/local/path/to/file.txt"
destination = "/path/to/file.txt"

}

###############################################################################
# local executions and remote executions
#############################################################################

provisioner "local-exec" {
   command = "local commands here"
}

provisioner "remote-exec" {
   scripts = [ "list", "of", "local", "scripts"]
}

###############################################################################
# provisioner with inline
############################################################################

provisioner "remote-exec" {
  inline = [
      "sudo apt-get -y update"
      "sudo apt-get install tree"
  ]
}

##############################################################
# Defining the provisioner with in the resource block#
##############################################################

resource "aws_isntance" "example" {
   ami = "ami-id"
   instance_type = "t2.micro"

    provisione "local-exec" {
      command = "echo ${aws_instance.example.public_ip}" > ip_address.txt"
    }  
}

#######################################################################
# Provisioner with remote-exec #
#####################################################################

resource "aws_instance" "executing remote" {
  connection {
     host = "public Ip of the instance"
     user = "ubuntu"
     private_key = "private key path"
  }
provisioner "remote-exec" {
    inline = ["echo 'Connected to remote!'"]
}
  }
  
#########################################################################
provisioner "remote-exec" {
   inline = ["sudo apt-get -qq install python -y"]
 }
###################################################################

