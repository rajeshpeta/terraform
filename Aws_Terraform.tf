##################################
## variables
################################

variable "aws_access_key"  {
  default = "My_key"
}
variable "aws_secret_key" {
  default = "My_Key"
}

variable "private_key_path" {
   default = "/opt/keys/Awskey.pem"
}
variable "key_name" {
   default = "Awskey"
}

variable "region" {
  default = "ap-south-1"
}

variable "network_address_space" {
  default = "10.1.0.0/16"
}

variable "subnet1_address_space" {
  default = "10.1.0.0/24"
}


 # Define the Provider/Cloud Provider

 provider "aws" {
   access_key = var.aws_access_key
   secret_key = var.aws_secret_key
   region = var.region
 }

#########################################################
# Data #
#########################################################

data "aws_availability_zones" "available" {}

data "aws_ami" "aws-ubuntu" {
most_recent = true
owners = ["amazon"]

filter {

  name = "name"
  values = ["amzn-ami-hvm*"]
}

filter {

  name = "root-device-type"
  values = ["ebs"]
}

filter {

  name = "virtualization-type"
  values = ["hvm"]
}
}

##########################################################
# Resources
##########################################################

# NETWORKING #

resource "aws_vpc" "Terr_VPC" {
   cidr_block = var.network_address_space
   enable_dns_hostnames = "true"
}

resource "aws_internet_gateway" "igw" {
     vpc_id = aws_vpc.Terr_VPC.id
}

resource "aws_subnet" "subnet1" {
    cidr_block = var.subnet1_address_space
    vpc_id = aws_vpc.Terr_VPC.id
    map_public_ip_on_launch = "true"
    availability_zone = data.aws_availability_zones.available.names[0]
}

 # Routing #

 resource "aws_route_table" "rtb" {
   vpc_id = aws_vpc.Terr_VPC.id

         route {
            cidr_block = "0.0.0.0/0"
            gateway_id = aws_internet_gateway.igw.id
         }
}

resource "aws_route_table_association" "rta-subnet1" {
    subnet_id  = aws_subnet.subnet1.id
    route_table_id = aws_route_table.rtb.id
}

resource "aws_security_group" "Terr_sg" {
    name = "Terr_sg"
    vpc_id = aws_vpc.Terr_VPC.id

# SSH Access from anywhere#

  ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
}

# Http access from anywhere #

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
}

# outbound internet access #

  egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   }
}


# Instnaces ##

     resource "aws_instance" "Dev1" {
     ami = "ami-0123b531fc646552f"
     instance_type = "t2.micro"
     subnet_id = aws_subnet.subnet1.id
     vpc_security_group_ids = [aws_security_group.Terr_sg.id]
     tags = {
       Name = "Dev1"
     }

     key_name = var.key_name

     connection {
        type = "ssh"
        host = self.public_ip
        user = "ubuntu"
        private_key = file(var.private_key_path)
     }
provisioner "remote-exec" {
   inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install tree"
    ]
}
}

#########################################################
# OUTPUT
###########################################################
 output "aws_instance_public_dns" {
    value = aws_instance.Dev1.public_dns

 }
